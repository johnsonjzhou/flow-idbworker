/**
 * Handles IndexedDB transactions with Promises and in a Web Worker thread
 * @author  Johnson Zhou <johnson@simplyuseful.io>
 * 
 * @export  IDBWorker as default
 * 
 * todo -- IDBKeyRange queries
 * todo -- multiple transaction (iterative)
 * todo -- openCursor
 * todo -- openKeyCursor
 */
import { registerWorker } from 'flow-utils';
import { idb_web_worker } from './idb_web_worker';

/**
 * Interface class on the main thread
 */
class IDBWorker {

  /**
   * @param  {object}  schema
   * @see  sample_schema
   */
  constructor(schema = {}) {

    // db schema
    this.schema = schema;

    // methods
    this.registerWorker = this.registerWorker.bind(this);
    this.terminateWorker = this.terminateWorker.bind(this);
    this.handleWorkerMessage = this.handleWorkerMessage.bind(this);
    this.open = this.open.bind(this);
    this.error = this.error.bind(this);
    this.response = this.response.bind(this);

    // web worker
    this.worker = null;

    // promise handlers
    this.dbResolve = null;
    this.dbReject = null;
    this.txnResolve = null;
    this.txnReject = null;
  }

  /**
   * @private
   * Registers the web worker
   */
  registerWorker() {
    this.worker = registerWorker(idb_web_worker);
    this.worker && (this.worker.onmessage = this.handleWorkerMessage);
    this.worker && this.worker.postMessage({ 
      type: 'schema', 
      payload: this.schema 
    });
  }

  /**
   * @private
   * Terminates the web worker
   */
  terminateWorker() {
    this.worker && this.worker.terminate();
  }

  /**
   * @private
   * Handle message received from worker thread
   * @param  {event}  event
   * @param  {object}  event.data
   * @param  {string}  event.data.type
   * @param  {bool}  event.data.ok
   * @param  {mixed}  event.data.payload
   */
  handleWorkerMessage({ data }) {
    const {
      type, 
      ok,
      payload 
    } = data;

    // log if error occurred
    (!ok) && window.Console && window.Console.notify(
      `Error occurred in ${type}\n${payload}`, 
      'IDBWorker'
    );

    switch (type) {
      case 'open':
        ok && this.dbResolve(payload) || this.dbReject(payload);
      return;
      case 'upgrade':
        !ok && this.dbReject(payload);
      return;
      case 'bubble':
        // error that has bubbled
        // todo: needs testing
        !ok && this.txnReject(payload);
      return;
      default:
        ok && this.txnResolve(payload) || this.txnReject(payload);
      return;
    }
  }

  /**
   * @private
   * Opens the database in the worker thread
   */
  open() {
    this.registerWorker();

    return new Promise((resolve, reject) => {
      // bind resolve or reject
      this.dbResolve = resolve;
      this.dbReject = reject;

      // open the DB
      this.worker && this.worker.postMessage({ type: 'open' });
    });
  }

  /**
   * @private
   * Conducts the database transaction in the worker thread
   * @param  {object}  object
   * @param  {string}  object.store - object store name
   * @param  {string}  object.type - request type, eg. add, get, etc
   * @param  {string}  object.mode - transaction mode, eg readwrite
   * @param  {mixed}   object.payload - the argument to pass to the request method
   * 
   * @fires  Promise.reject
   * @fires  this.worker.postMessage
   * 
   * @see  idb_web_worker.transact
   */
  transact({ store, type, mode, payload }) {
    return new Promise((resolve, reject) => {
      // bind resolve or reject
      this.txnResolve = resolve;
      this.txnReject = reject;

      // reject if missing any of the required parameters
      !(store && type && mode) && reject(
        'Missing transaction parameters'
      );

      //todo -- IDBKeyRange
      (typeof payload === 'object' && payload.constructor.name === 'IDBKeyRange')
      && reject('IDBKeyRange could not be used in this instance');

      // send transaction request to worker thread
      try {
        this.worker && this.worker.postMessage({
          store, type, mode, payload
        });
      } catch (error) {
        reject(`${error.name}\n${error.message}`);
      }
    });
  }

  /**
   * @private
   * Creates an error response object
   * @fires  this.response
   */
  error(reason) {
    return this.response({ ok: false, payload: reason });
  }

  /**
   * @private
   * Terminates the worker then returns a response object
   * @return  {object}   response
   * @return  {bool}     response.ok
   * @return  {mixed}    response.payload
   * @return  {function} response.transaction -- instance of this.transaction
   * 
   * @fires  this.terminateWorker
   */
  response({ ok = true, payload = null }) {
    this.terminateWorker();
    return { ok, payload, transaction: this.transaction.bind(this) };
  }

  /**
   * @public
   * Enacts an IndexedDB transaction by,
   * opening the database, then
   * carrying out the transaction
   * 
   * @return  {Promise}  - resolves to an response object
   * @see  this.response
   * 
   * @fires  this.open
   */
  transaction({ 
    store, 
    type, 
    mode = 'readwrite', 
    payload 
  }) {
    return this.open()
    .then(
      // on DB open
      () => {
        return this.transact.call(this, { store, type, mode, payload })
        .then(
          // on txn success
          (payload) => this.response ({ ok: true, payload }), 

          // on txn failure
          this.error
        );
      },

      // on DB error
      this.error
    );
  }
}

export {
  IDBWorker as default,
};
