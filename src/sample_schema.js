/**
 * A sample schema demonstrating how the schema object works with IDBWorker
 * @author  Johnson Zhou <johnson@simplyuseful.io>
 * 
 * @export  sample_schema
 * @export  sample_data
 */

const sample_schema = {
  db: {
    name: 'SAMPLE_DATABASE',
    version: 1,
  },
  stores: [
    {
      name: 'PRODUCTS',
      keyPath: 'product_id',
      autoIncrement: false,
      // deprecate: false,
      indexes: [
        {
          indexName: 'id',
          keyPath: 'product_id',
          objectParameters: { unique: true },
          // deprecate: false
        },
        {
          indexName: 'name',
          keyPath: 'product_name',
        }
      ]
    },
  ]
};

const sample_data = [
  {
    product_id: 1234,
    name: 'Keyboard',
    price: 12.99,
  },
  {
    product_id: 4353,
    name: 'Mouse',
    price: 4.99,
  },
  {
    product_id: 1234,
    name: 'Monitor',
    price: 8.99,
  },
]

export { sample_schema, sample_data };