/**
 * The web worker to handle IndexedDB transactions for use with IDBWorker
 * @author  Johnson Zhou <johnson@simplyuseful.io>
 * 
 * @export  idb_web_worker
 */
import { CustomWorker } from 'flow-utils';

const idb_web_worker = new CustomWorker();

/**
 * @note 
 * Worker variable references:
 * 
 * self.schema - the database schema
 * self.db - the IDBDatabase object
 * self.txnRequest - an IDBRequest object stemming from a database transaction
 */

/**
 * Handle message received from main thread
 * @param  {event}  event
 * @param  {object} event.data
 */
idb_web_worker.onmessage = function({ data }) {
  const { 
    store, 
    type, 
    mode, 
    payload
  } = data;

  switch (type) {
    case 'schema':
      self.schema = payload;
    return;
    case 'open':
      self.openDB();
    return;
    default:
      self.transact({ store, type, mode, payload });
    return;
  }
};

/**
 * Opens the database
 * @throws  {TypeError}
 */
idb_web_worker.openDB = function() {
  try {
    // respond with error if indexedDB is not available
    if (!self.indexedDB) 
      throw new TypeError('IndexedDB not supported');

    // respond with error if db not specified in schema
    if (!(self.schema && self.schema.db)) 
      throw new TypeError('DB not defined in schema');

    // get DB name and version from schema
    const {
      name, version
    } = self.schema.db;

    const openRequest = self.indexedDB.open(name, version);
    openRequest.onsuccess = self.onDBOpen;
    openRequest.onupgradeneeded = self.onDBUpgrade;
    openRequest.onerror = self.onDBError;

  } catch (error) {
    self.notifyError({
      type: 'open',
      payload: `${error.name}\n${error.message}`,
    })
  }
};

/**
 * Handles IDBOpenDBRequest.onupgradeneeded, 
 * will create or upgrade object stores and/or indexes
 * @param  {Event}  event -- IDBOpenDBRequest
 * @throws  {TypeError}
 * 
 * @see  sample_schema
 */
idb_web_worker.onDBUpgrade = function(event) { 
  try {
    // get the database and transaction references
    const {
      result: db,
      transaction
    } = event.target;
    
    // create objectStores from schema
    const {
      stores = []
    } = self.schema;

    if (!Array.isArray(stores)) 
      throw new TypeError('Schema.stores expected to be an array');

    stores.forEach(store => { 
      const {
        name, 
        keyPath, 
        autoIncrement, 
        indexes = [], 
        deprecate = false, 
      } = store;

      // objectStore name is required as a minimum
      if (!name) 
        throw new TypeError('Object store name is required in schema');

      // optionalParameters
      let options = {};
      keyPath && (options.keyPath = keyPath);
      autoIncrement && (options.autoIncrement = autoIncrement);

      // create the objectStore if not existing
      const existingStore = 
        db.objectStoreNames && db.objectStoreNames.contains(name) || false;
      const objectStore = 
        (existingStore && transaction.objectStore(name)) ||
        db.createObjectStore(name, options);
      
      // delete the store if existing and marked to deprecate
      if (existingStore && deprecate) {
        return db.deleteObjectStore(name);
      }

      // create indexes from schema 
      if (!Array.isArray(indexes)) 
        throw new TypeError('Schema.stores.index expected to be an array');

      indexes.forEach(index => {
        const {
          indexName,
          keyPath, 
          objectParameters, 
          deprecate = false, 
        } = index;

        // indexName and keyPath is required as a minimum
        if (!(indexName && keyPath)) 
          throw new TypeError('Index name and keypath required in schema');

        // create the index or delete an existing deprecated index
        const existingIndex = 
          objectStore.indexNames && objectStore.indexNames.contains(indexName) || 
          false;

        // create the index if not existing
        (!existingIndex) && 
          objectStore.createIndex(indexName, keyPath, objectParameters);

        // delete the index if existing and marked to deprecate
        if (existingIndex && deprecate) {
          return objectStore.deleteIndex(indexName);
        }
      });
    });
    
  } catch (error) {
    self.notifyError({
      type: 'upgrade',
      payload: `${error.name}\n${error.message}`,
    });
  }
};

/**
 * General database error handling, fired during database 'open' or
 * caught when an error 'bubble'(s) from an uncaught transaction
 * @param  {Event}  event - type = error
 * @fires  self.notifyError
 * 
 * todo: test 'bubble' 
 */
idb_web_worker.onDBError = function(event) {
  const { 
    constructor, 
    error 
  } = event.target;

  // differentiate between error when opening DB
  // or error that has bubbled
  //todo -- test this
  const type = (constructor.name === 'IDBOpenDBRequest') && 'open' || 'bubble';
  return self.notifyError({ type, payload: `${error.name}\n${error.message}` });
};

/**
 * Handles IDBOpenDBRequest.onsuccess (open database) event,
 * assigns the database reference object in self.db,
 * notifies main thread that database is open and ready
 * @param  {Event}  event
 * @fires  self.notifyOk
 */
idb_web_worker.onDBOpen = function(event) {
  // @global, db reference object that can be used for transactions
  self.db = event.target.result;

  // notify main thread
  return self.notifyOk({ type: 'open' });
};

/**
 * Conducts the requested database transaction by,
 * establishing a database transaction,
 * opening the relevant object store,
 * conducting a relevant request type on the object store.
 * @param  {object}  object
 * @param  {string}  object.store - object store name
 * @param  {string}  object.type - request type, eg. add, get, etc
 * @param  {string}  object.mode - transaction mode, eg readwrite
 * @param  {mixed}   object.payload - the argument to pass to the request method
 * 
 * @fires  self.notifyError
 * 
 * @throws  {TypeError}
 * 
 * @see  IDBWorker.transact
 * 
 * todo -- IDBKeyRange queries
 */
idb_web_worker.transact = function({ store, type, mode, payload }) {
  try {
    // reject if DB is not yet open
    if (!self.db) throw new TypeError('Database is not yet open.');

    // create the transaction
    const transaction = self.db.transaction([store], mode);
    if (!(transaction && transaction.constructor.name === 'IDBTransaction'))
      throw new TypeError('Could not create IDBTransaction object');

    // bind complete and error handlers
    transaction.oncomplete = self.onTransactComplete.bind(type);
    transaction.onerror = self.onTransactError.bind(type);

    // execute the transaction
    const objectStore = transaction.objectStore(store);
    if (!(objectStore && objectStore.constructor.name === 'IDBObjectStore'))
      throw new TypeError(
        `Could not access object store [${store}] in transaction`
      );
    
    switch(type) {
      case 'add':
        return self.txnRequest = objectStore.add(payload);
      case 'put':
        return self.txnRequest = objectStore.put(payload);
      case 'get':
        return self.txnRequest = objectStore.get(payload);
      case 'getAll':
        //todo -- IDBKeyRange
        return self.txnRequest = objectStore.getAll();
      case 'delete':
        return self.txnRequest = objectStore.delete(payload);
      case 'clear':
        return self.txnRequest = objectStore.clear();
      case 'count':
        return self.txnRequest = objectStore.count();
      case 'getKey':
        //todo -- IDBKeyRange
        return self.txnRequest = objectStore.getKey(payload);
      case 'getAllKeys':
        //todo -- IDBKeyRange
        return self.txnRequest = objectStore.getAllKeys();
      case 'index':
        return self.txnRequest = objectStore
          .index(payload)
          .getAll();
      default:
        throw new TypeError(`Unknown transaction type [${type}]`);
    };

  } catch (error) {
    self.notifyError({
      type, 
      payload: `${error.name}\n${error.message}`,
    })
  }
};

/**
 * Handles IDBTransaction.oncomplete (transaction) event
 * @param  {IDBRequest}  self.txnRequest
 * @fires  self.notifyOk
 * @fires  self.notifyError
 * @this  type
 */
idb_web_worker.onTransactComplete = function() {
  // identify the transaction type
  const txnType = self.txnRequest.constructor.name;

  switch (txnType) {
    // return the result from the transaction
    case 'IDBRequest':
      const {
        result
      } = self.txnRequest;
      return self.notifyOk({ type: this.toString(), payload: result });

    // return an error if the completed transaction type is not expected
    default:
      return self.notifyError({ 
        type: this.toString(), 
        payload: `TypeError\nTransaction completed with a [${txnType}]` 
      });
  }
}

/**
 * Handles IDBTransaction.onerror (transaction) event
 * @param  {Event}
 * @fires  self.notifyError
 * @this  type
 */
idb_web_worker.onTransactError = function(event) {
  const {
    error
  } = event.target;
  self.notifyError({
    type: this.toString(), 
    payload: `${error.name}\n${error.message}`
  });
}

/**
 * Posts a message to the main thread with an ok=true state
 * @param  {object}  message
 * @param  {string}  message.type
 * @param  {mixed}   message.payload
 * @fires  self.postMessage
 */
idb_web_worker.notifyOk = function({ type, payload }) {
  return self.postMessage({ type, ok: true, payload }); 
}

/**
 * Posts a message to the main thread with an ok=false state
 * @param  {object}  message
 * @param  {string}  message.type
 * @param  {mixed}   message.payload
 * @fires  self.postMessage
 */
idb_web_worker.notifyError = function({ type, payload }) {
  return self.postMessage({ type, ok: false, payload }); 
}

export {
  idb_web_worker 
};