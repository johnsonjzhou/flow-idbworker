# IDBWorker

Handles IndexedDB transactions with Promises and in a Web Worker thread.  

Supported IDBObjectStore methods:  
- add, put, get, getAll, delete, clear, count, getKey, getAllKeys, index  

Limitations and future work:  
- `IDBKeyRange` based queries currently not supported. 
As all IndexedDB methods are carried out exclusively in the web worker thread, 
and it is not possible to pass the `IDBKeyRange` object via `worker.postMessage`.  

- Carry out multiple transactions based on iterative dataset.  

- Support for `openCursor` and `openKeyCursor`.  

---
## Install
````bash
npm install https://bitbucket.org/johnsonjzhou/flow-idbworker.git
````

---
## Use
````javascript
import IDBWorker, { sample_schema, sample_data } from 'flow-idbworker';
````

---
## Syntax
````javascript
// called as constructor
const idb = new IDBWorker([schema]);
````

---
## Parameters

***schema***  
An object describing the IndexdedDB database schema. 
Refer to [sample_schema](./src/sample_schema.js).  

---
## Methods of `IDBWorker` constructor
- IDBWorker.transaction

---
## IDBWorker.***transaction***

### Syntax
````javascript
idb.transaction({ store, type, mode, payload })
.then((response) => { });
````

### Parameters
***store*** `string`  
Name of the `IDBObjectStore` to access.  

***type*** `string`  
Name of the `IDBObjectStore` request method to use.  

***mode*** `string`  
The types of access that can be performed in the transaction. 
Transactions are opened in one of three modes: 
`readonly`, `readwrite` and `readwriteflush`. Default is `readwrite`.

***payload*** `mixed`  
The argument to pass to the `IDBObjectStore` request method. 
This may be any value or JavaScript object handled by the structured `clone` 
algorithm, which includes cyclical references.  

### Return value(s)
A thenable `Promise` that resolves to a `response` object.  

***response.ok*** `bool`  
Whether the transaction was successful.  

***response.payload*** `mixed`  
Any return values obtained from the transaction method.  

***response.transaction*** `function`  
An instance of `IDBWorker.transaction`, that allows the process to be 
chained further.  

---
## Examples
````javascript
import IDBWorker, { sample_schema, sample_data } from 'flow-idbworker';

const idb = new IDBWorker(sample_schema);

idb.transaction({
  store: 'PRODUCTS',
  type: 'put',
  mode: 'readwrite',
  payload: sample_data[0]
})
.then(response => {
  console.log(response)
  // Object { ok: true, payload: 1234, transaction: transaction() }

  return response.transaction({
    store: 'PRODUCTS',
    type: 'index',
    mode: 'readwrite',
    payload: 'id'
  });
})
.then(response => console.log(response));
// Object { ok: true, payload: (1) […], transaction: transaction() }
````

---
## License
MIT.  
Johnson Zhou [johnson@simplyuseful.io](mailto://johnson@simplyuseful.io).  